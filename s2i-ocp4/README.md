# Intro
Just some sample to kick start a python app on openshift
### But first we try run it on standalone
Execute the runme.sh script to start the server
Then open up a browser and point to the server:5000. Make sure to use port number 5000
### Then deploy it into openshift platform
oc new-app --name helloworld https://github.com/jasonwcc/learntoopenshift --context-directory=s2i-ocp4 --as-deployment-config
oc get dc
oc get pods
 
