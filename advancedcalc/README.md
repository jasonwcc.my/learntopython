# power-of.py  - calculate Power Of / Exponential
### Method 1: Basic Approach
The very basic method to compute a^n is to multiply the number a, n times again and again. This approach is pretty slow and not at all efficient.

### Method 2: Normal Recursive Approach
We will be approaching this method through Recursion. If you want to know more about Recursion you can read the tutorial mentioned below.

Read more about Recursion: Recursion in Python

Here the basic concept is that fun(a,n) = a * fun(a,n-1). So recursion can be used for computing a raised to the power n.

### Method 3: Fast Recursive Approach
Earlier we used a linear recursive approach but to compute a raise to the power of n can also be computed on the basis of the value of n ( power value).


