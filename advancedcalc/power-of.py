# Method 1 : Basic 
def basic_approach(a,n):
  ans = 1
  for i in range(n):
    ans *=a
  return ans

def normal_recursion(a,n):
    # If power is 0 : a^0 = 1
    if(n==0):
        return 1
    # If power is 1 : a^1 = a
    elif(n==1):
       return a 
    
    # For >=2 : a^n = a* (a^(n-1))
    term = normal_recursion(a,n-1)
    term = a * term

    # Return the answer
    return term

# Method 3 : Fast Recursive approach
def fast_recursion(a,n):
         
  # If power is 0 : a^0 = 1
  if(n==0):
    return 1
                         
  # If power is 1 : a^1 = a
  elif(n==1):
    return a
                                     
  # For n>=2 : n can be even or odd
                                         
  # If n is even : a^n = (a^(n/2))^2
  # if n is odd : a^n = a * ((a^(n/2))^2)
                                                      
  # In both the cases we have the calculate the n/2 term
  term = fast_recursion(a,int(n/2))
  term *= term
                                                                   
  # Now lets check if n is even or odd
  if(n%2==0):
    return term
  else:
    return a*term
                                                                                                
#print(basic_approach(2,5))
#print(normal_recursion(2,5))
print(fast_recursion(2,5))
	
